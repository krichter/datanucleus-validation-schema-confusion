package richtercloud.datanucleus.validation.schema.confusion;

import java.io.Serializable;
import java.util.ArrayList;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.validation.constraints.Size;

/**
 *
 * @author richter
 */
@Entity
public class MyEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    public static final int IMAGE_COUNT_MAX = 5;
    @Id
    @GeneratedValue
    private Long id;
    @Size(max = IMAGE_COUNT_MAX, message = "Maximal " + IMAGE_COUNT_MAX + " images are allowed")
    @Basic
    @Lob
    private ArrayList<byte[]> imageData = new ArrayList<>();

    public MyEntity() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ArrayList<byte[]> getImageData() {
        return imageData;
    }

    public void setImageData(ArrayList<byte[]> imageData) {
        this.imageData = imageData;
    }
}
