package richtercloud.datanucleus.validation.schema.confusion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author richter
 */
public class NewMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        Locale.setDefault(Locale.US);
        String driver = "org.apache.derby.jdbc.EmbeddedDriver";
        Class.forName(driver).newInstance();
        String protocol = "jdbc:derby:";
        Connection conn = DriverManager.getConnection(protocol + "datanucleus;create=true");
        conn.close();

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("richtercloud_datanucleus-validation-schema-confusion_jar_1.0-SNAPSHOTPU");
        EntityManager em = emf.createEntityManager();
        MyEntity myEntity = new MyEntity();
        em.getTransaction().begin();
        em.persist(myEntity);
        em.flush();
        em.getTransaction().commit();
        em.close();
    }
    
}
